package com.solis.tareaestructura.Disculpeporelretardo;

/**
 *
 * @author Solis
 */
public class Grafos {
    public static Graph getCities(){
        Nodo xba = new Nodo("XBACAB");
        Nodo cha = new Nodo("CHAMPOTON");
        Nodo sey = new Nodo("SEYBAPLAYA");
        Nodo mer = new Nodo("MERIDA");
        Nodo car = new Nodo("CARMEN");
        Nodo ler = new Nodo("LERMA");
        Nodo chi = new Nodo("CHINA");
        Nodo cam = new Nodo("CAMPECHE");
        

        
        
        xba.addEdge(new Edge(xba, cha, 44.29));
        xba.addEdge(new Edge(xba, sey, 77.83));
        xba.addEdge(new Edge(xba, mer, 253));
        xba.addEdge(new Edge(xba, car, 115));
        xba.addEdge(new Edge(xba, ler, 97));
        xba.addEdge(new Edge(xba, chi, 94));
        xba.addEdge(new Edge(xba, cam, 96));
 
        Graph graph = new Graph();
        graph.addNodo(xba);
        graph.addNodo(cha);
        graph.addNodo(sey);
        graph.addNodo(mer);
        graph.addNodo(car);
        graph.addNodo(ler);
        graph.addNodo(chi);
        graph.addNodo(cam);

        return graph;

 
    }
            
            
    public static void main(String[] args) {
        
        Graph graph = getCities();
        System.out.println(graph);
    }
    
}

